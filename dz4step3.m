function [sum_kl_err]=dz4step3(minrasst,minim,k)
	kl_err=[];
	for h=1:k
		kl_err=[kl_err sum(minrasst(minim(:,h),1))];
	end
	sum_kl_err=sum(kl_err)



