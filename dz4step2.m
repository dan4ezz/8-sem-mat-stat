function [a,tochki,minrasst,minim]=dz4step2(a,tochki,n,klust,rep,colors)
	rasst=ones(n,klust);  %distance matrix
	o1=rep;	
	for o0=1:o1
		for i1=1:klust
			rasst(:,i1)=(a(:,1).-tochki(i1,1)).^2+(a(:,2).-tochki(i1,2)).^2; %distance from the points to the centroids [100*klust x klust]
		end
		minrasst=repmat(min(rasst')', [1,klust]); %minimum distance
		minim=(rasst==minrasst);
		tochki(:,1)=(sum(repmat(a(:,1),[1 klust]).*minim))'./(sum(minim))'; %new centriods(x)
		tochki(:,2)=(sum(repmat(a(:,2),[1 klust]).*minim))'./(sum(minim))'; %new centriods(y)
		tochki(isnan(tochki), 1)=(50*rand(klust,1)+a(n/2, 1))(isnan(tochki), 1);  %remove 0
		tochki(isnan(tochki), 2)=(50*rand(klust,1)+a(n/2, 2))(isnan(tochki), 2);  %remove 0
		%if(debug==1)
		%	okno=figure();		
		%	hold on
		%	for g=0:(k-1)
		%		plot(a'(1,(minim(:,(g+1)))'),a'(2,(minim(:,(g+1)))'), [colors(g+1) "x"]);  %draw points
		%	end
		%	plot(tochki'(1,:),tochki'(2,:), 'b*'); %draw centroids
		%	hold off
		%	input("next - 1 ");
		%	delete(okno);
		%endif
	end
	%if(debug==1)
		okno=figure();
		hold on
		for g=0:(klust-1)
			plot(a'(1,(minim(:,(g+1)))'),a'(2,(minim(:,(g+1)))'), [colors(g+1) "."]);  %draw points
		end
		plot(tochki'(1,:),tochki'(2,:), 'kx');
		hold off
		input("next - 1 ");
		delete(okno);
	%endif
