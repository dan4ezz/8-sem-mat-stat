init_data;
size(data1, 2)
N = 66; %моя выборка
a=[];
%for i=1:N
%	do
%		val = floor(400*rand+1);
%	until (size(find(a==val), 2) == 0)
%	a = [a val];
%endfor

a =  [153 284 389 47 128 355 115 68 367 331 6 315 155 167 265 29 328 10 38 89 232 377 143 190 209 105 55 333 207 181 160 36 218 7 299 93 380 351 80 242 269 130 290 174 103 304 390 343 69 336 376 317 54 177 205 352 192 282 357 168 187 309 350 17 279 303];


%Пункт 1
viborka1=data1(1,a(1,:));
viborka2=data2(1,a(1,:));

viborka = [viborka1; viborka2];

format short g;

%viborka = [156.800    93.600   117.500   141.200   143.900   137.500   100.000   134.200   117.900    98.800   181.000   155.500   136.700   121.200   120.900   140.700   147.900   156.500   153.600   146.100   139.700   150.900   116.100   136.700   134.900   131.100   109.000   162.300   146.000   124.700   137.400    98.300   137.200   155.700   137.900   112.900   146.800   119.500   131.000   179.000   97.800   145.300   124.400   119.700   112.400   118.000   148.200   124.300   135.100    95.800   109.800   151.200   144.900   114.600   147.300   158.400   104.700   126.700    84.900   117.800   127.700   136.800   112.000   131.100   144.300    89.200	170.100];

%2) Построение рейнжированного ряда
printf("\npynkt 2\n");
rengirovanniy = sort(viborka1)
rengirovanniy2 = sort(viborka2); %22222222222222222222
razmah = rengirovanniy(1, end) - rengirovanniy(1, 1)
razmah2 = rengirovanniy2(1, end) - rengirovanniy2(1, 1); %22222222222222222
N=size(viborka1, 2)

%3) Разбиение на интервалы
printf("\npynkt 3\n");
k = 1 + log2(N);
k=floor(k);
if (rem(k,2)==0)
	k += 1;
endif
k
h = razmah/k
h2 = razmah2/k; %222222222222222

%4) Интервальный ряд (матрица 3хN, 1 строка - правые границы интервалов, 2 строка - середины интервалов, 3 строка - кол-во попаданий в интервал, 4 строка - относительная частота)
printf("\npynkt 4\n");
intervalniy = 1:k;
intervalniy2 = 1:k; %222222222222
intervalniy = [intervalniy; intervalniy];
intervalniy2 = [intervalniy2; intervalniy2]; %222222222222222
intervalniy(1,:) = rengirovanniy(1,1) + [1:k].*h;
intervalniy(2,:) = rengirovanniy(1,1) + ([1:k]-0.5).*h;
intervalniy2(1,:) = rengirovanniy2(1,1) + [1:k].*h2;   %2222222222222
intervalniy2(2,:) = rengirovanniy2(1,1) + ([1:k]-0.5).*h2;  %22222222222222
temp = ones(1, k);
temp(1,1) = sum(rengirovanniy <= intervalniy(1,1));
temp2 = ones(1, k); %2222222222222
temp2(1,1) = sum(rengirovanniy2 <= intervalniy2(1,1)); %222222222222222222
for i=2:k
	temp(1, i) = sum(and(rengirovanniy <= intervalniy(1,i), rengirovanniy > intervalniy(1,i-1)));
	temp2(1, i) = sum(and(rengirovanniy2 <= intervalniy2(1,i), rengirovanniy2 > intervalniy2(1,i-1)));   %222222222222
endfor
intervalniy = [intervalniy; temp; temp/N]
intervalniy2 = [intervalniy2; temp2; temp2/N]; %222222222222

%5) Относительные частоты
printf("\npynkt 5\n");
otnosit_chast = 1:k;
otnosit_chast = [otnosit_chast; intervalniy(2,:); intervalniy(3,:)]
otnosit_chast2 = 1:k;  %2222222222
otnosit_chast2 = [otnosit_chast2; intervalniy2(2,:); intervalniy2(3,:)];  %2222222222

%6) Полигон частот
printf("\npynkt 6\n");
polygon_chast = ones(1, k);
polygon_chast(1,1) = sum(rengirovanniy(rengirovanniy(1,:) <= intervalniy(1,1)))/intervalniy(3,1);
polygon_chast2 = ones(1, k); %222222222
polygon_chast2(1,1) = sum(rengirovanniy2(rengirovanniy2(1,:) <= intervalniy2(1,1)))/intervalniy2(3,1);  %2222222222
for i=2:k
	polygon_chast(1, i) = sum(rengirovanniy(and(rengirovanniy <= intervalniy(1,i), rengirovanniy > intervalniy(1,i-1))))/intervalniy(3,i);
	polygon_chast2(1, i) = sum(rengirovanniy2(and(rengirovanniy2 <= intervalniy2(1,i), rengirovanniy2 > intervalniy2(1,i-1))))/intervalniy2(3,i);  %2222222
endfor
polygon_chast = [polygon_chast; intervalniy(3,:)]
polygon_chast2 = [polygon_chast2; intervalniy2(3,:)]; %22222222222
plot(polygon_chast(1,:), polygon_chast(2,:), "k");
input("Для продолжения нажмите че нить");

%7) Гистограмма
printf("\npynkt 7\n");
hist(viborka1, k, "k");
input("Для продолжения нажмите че нить");

%8) Эмпирическая функция распределения
printf("\npynkt 8\n");
emp_f_r = [1; sum(rengirovanniy<=intervalniy(1, 1))];
emp_f_r2 = [1; sum(rengirovanniy2<=intervalniy2(1, 1))]; %22222222222222
plot([rengirovanniy(1,1) intervalniy(1, 1)], [sum(rengirovanniy<=intervalniy(1, 1)) sum(rengirovanniy<=intervalniy(1, 1))]/N, "k");
hold on;
for i=2:k
	emp_f_r = [emp_f_r [i; sum(rengirovanniy<=intervalniy(1, i))]];
	emp_f_r2 = [emp_f_r2 [i; sum(rengirovanniy2<=intervalniy2(1, i))]];  %22222222222
	plot([intervalniy(1, i-1) intervalniy(1, i)], [sum(rengirovanniy<=intervalniy(1, i)) sum(rengirovanniy<=intervalniy(1, i))]/N, "k") ;
endfor
hold off;
emp_f_r = [emp_f_r; emp_f_r(2,:)/N]
emp_f_r2 = [emp_f_r2; emp_f_r2(2,:)/N]
input("Для продолжения нажмите че нить");

%9) Выборочное среднее
printf("\npynkt 9\n");
printf("\nstat ryad:\n");
vib_sr = sum(viborka1) / N
vib_sr2 = sum(viborka2) / N;  %22222222222
printf("\nintervalniy ryad:\n");
vib_sr_int = sum(intervalniy(2,:).*intervalniy(4,:))
vib_sr_int2 = sum(intervalniy2(2,:).*intervalniy2(4,:)); %22222222

%10) Медиана интервальной выборки (В интервальную выборку добавляется 5 строка, обозначающая накопленную частоту)
printf("\npynkt 10\n");
printf("\nstat ryad(tolko dl9 ne4etnogo obyoma viborki):\n"); %работает только с нечетной выборкой
mediana_st = rengirovanniy(1, floor(N/2)+1)
mediana_st2 = rengirovanniy2(1, floor(N/2)+1); %22222222222
printf("\nintervalniy ryad:\n");
med_number = floor(k/2) + 1; %номер среднего интервала
temp = ones(1, k);
temp2 = ones(1, k);  %222222222
for i=1:k
	temp(1, i) = sum(rengirovanniy <= intervalniy(1,i));
	temp2(1, i) = sum(rengirovanniy2 <= intervalniy2(1,i));  %22222222222222
endfor
intervalniy = [intervalniy; temp];
intervalniy2 = [intervalniy2; temp2]; %2222222222
mediana = intervalniy(1, med_number-1) + (intervalniy(1, med_number) - intervalniy(1, med_number-1))*((N/2 - intervalniy(4, med_number - 1)) / intervalniy(3, med_number))
mediana2 = intervalniy2(1, med_number-1) + (intervalniy2(1, med_number) - intervalniy2(1, med_number-1))*((N/2 - intervalniy2(4, med_number - 1)) / intervalniy2(3, med_number)); %222222

format short;
%11) Мода
printf("\npynkt 11\n");
printf("\nstat ryad:\n");
statisticheskiy=[];
statisticheskiy(1,1)=rengirovanniy(1,1);
statisticheskiy(2,1)=1;
statisticheskiy2=[];  %222222
statisticheskiy2(1,1)=rengirovanniy2(1,1);   %2222
statisticheskiy2(2,1)=1;  %2222
for i=2:N
	if (statisticheskiy(1, end) != rengirovanniy(1, i))
		statisticheskiy=[statisticheskiy [rengirovanniy(1, i); 0]];
		statisticheskiy(2,end)=1;			
	else
		statisticheskiy(2,end)=statisticheskiy(2,end)+1;
	endif
	if (statisticheskiy2(1, end) != rengirovanniy2(1, i))   %222222
		statisticheskiy2=[statisticheskiy2 [rengirovanniy2(1, i); 0]];   %22222
		statisticheskiy2(2,end)=1;			%22222
	else  %22222
		statisticheskiy2(2,end)=statisticheskiy2(2,end)+1; %22222
	endif  %2222222

endfor
N_stat = size(statisticheskiy,2);
N_stat2 = size(statisticheskiy2,2); %22222
printf("Modi:\n");
statisticheskiy(1,[statisticheskiy == max(statisticheskiy(2,:))](2,:))
maximums = statisticheskiy == max(statisticheskiy(2,:));
maximums(1,:) = maximums(2,:);
temp = statisticheskiy.*maximums;
moda = sum(temp(1,:))/sum(temp(2,:) != 0)
printf("\nintervalniy ryad:\n");
moda_num = otnosit_chast(1,intervalniy(3,:) == max(intervalniy(3,:)));
moda_int = intervalniy(1, moda_num - 1) + N*((intervalniy(3,moda_num)-intervalniy(3,moda_num-1))/((intervalniy(3,moda_num)-intervalniy(3,moda_num-1))+(intervalniy(3,moda_num)-intervalniy(3,moda_num+1))))
%22222222
statisticheskiy2(1,[statisticheskiy2 == max(statisticheskiy2(2,:))](2,:))
maximums2 = statisticheskiy2 == max(statisticheskiy2(2,:));
maximums2(1,:) = maximums2(2,:);
temp2 = statisticheskiy2.*maximums2;
moda2 = sum(temp2(1,:))/sum(temp2(2,:) != 0)
printf("\nintervalniy ryad:\n");
moda_num2 = otnosit_chast2(1,intervalniy2(3,:) == max(intervalniy2(3,:)));
moda_int2 = intervalniy2(1, moda_num2 - 1) + N*((intervalniy2(3,moda_num)-intervalniy2(3,moda_num2-1))/((intervalniy2(3,moda_num2)-intervalniy2(3,moda_num2-1))+(intervalniy2(3,moda_num2)-intervalniy2(3,moda_num2+1))))


%12) Выборочная дисперсия
printf("\npynkt 12\n");
format short g;
printf("\nstat ryad:\n");
vib_disp = sum((rengirovanniy - vib_sr).^2) / N
nesm_disp = sum((rengirovanniy - vib_sr).^2) / (N - 1)
printf("\nintervalniy ryad:\n");
vib_disp_int = sum(intervalniy(3,:).*(intervalniy(2,:) - vib_sr_int).^2) / N
nesm_disp_int = sum(intervalniy(3,:).*(intervalniy(2,:) - vib_sr_int).^2) / (N - 1)
format short;
%2222222222222
vib_disp2 = sum((rengirovanniy2 - vib_sr2).^2) / N;
nesm_disp2 = sum((rengirovanniy2 - vib_sr2).^2) / (N - 1);
vib_disp_int2 = sum(intervalniy2(3,:).*(intervalniy2(2,:) - vib_sr_int2).^2) / N;
nesm_disp_int2 = sum(intervalniy2(3,:).*(intervalniy2(2,:) - vib_sr_int2).^2) / (N - 1);


%13) Выборочное СКО
printf("\npynkt 13\n");
printf("\nstat ryad:\n");
vib_sko = sqrt(nesm_disp)
printf("\nintervalniy ryad:\n");
vib_sko_int = sqrt(nesm_disp_int)
%22222222
vib_sko2 = sqrt(nesm_disp2);
vib_sko_int2 = sqrt(nesm_disp_int2);


%14) Оценка ассиметрии и эксцесса
printf("\npynkt 14\n");
printf("\nstat ryad:\n");
format short g;
asimmet = sum((rengirovanniy - vib_sr).^3)/(N*vib_sko^3)
eksces = sum((rengirovanniy - vib_sr).^4)/(N*vib_sko^4) - 3
printf("\nintervalniy ryad:\n");
asimmet = sum(intervalniy(3,:).*(intervalniy(2,:) - vib_sr_int).^3)/(N*vib_sko_int^3)
eksces = sum(intervalniy(3,:).*(intervalniy(2,:) - vib_sr_int).^4)/(N*vib_sko_int^4) - 3
format short;
%222222222222
asimmet2 = sum((rengirovanniy2 - vib_sr2).^3)/(N*vib_sko2^3);
eksces2 = sum((rengirovanniy2 - vib_sr2).^4)/(N*vib_sko2^4) - 3;
asimmet_int2 = sum(intervalniy2(3,:).*(intervalniy2(2,:) - vib_sr_int2).^3)/(N*vib_sko_int2^3);
eksces_int2 = sum(intervalniy2(3,:).*(intervalniy2(2,:) - vib_sr_int2).^4)/(N*vib_sko_int2^4) - 3;

%15) Доверительный интервал для МО и СКО
printf("\npynkt 15\n"); %гама = 0.9
dov_int_mo = [vib_sr - 1.65*vib_sko/sqrt(N), vib_sr + 1.65*vib_sko/sqrt(N)]
dov_sko7 = [vib_sko*(1 - 0.179) vib_sko*(1 + 0.179)]

%16) Проверка гипотезы о нормальном законе по критерию Пирсона
printf("\npynkt 16\n");
pirs_tabl = [intervalniy(2,:); intervalniy(3,:); h*N.*exp(-((intervalniy(2,:) - vib_sr_int)/vib_sko_int).^2/2)/(vib_sko_int*sqrt(2*pi))]
XI =  sum((pirs_tabl(2,:) - pirs_tabl(3,:)).^2/pirs_tabl(3,:))

%17) Условные варианты
u_ysl_var = (intervalniy(2,:)'-131.6)/h;
ysl_variants = [intervalniy(2,:)', intervalniy(3,:)', u_ysl_var, u_ysl_var.*intervalniy(3,:)', u_ysl_var.^2.*intervalniy(3,:)', u_ysl_var.^3.*intervalniy(3,:)', u_ysl_var.^4.*intervalniy(3,:)', (u_ysl_var.^4+1).*intervalniy(3,:)'];
format bank;
ysl_variants=[ysl_variants; 0, sum(ysl_variants(:, 2)), 0, sum(ysl_variants(:, 4)), sum(ysl_variants(:, 5)), sum(ysl_variants(:, 6)), sum(ysl_variants(:, 7)), sum(ysl_variants(:, 8))]
format short g;
M1 = ysl_variants(8,4)/N
M2 = ysl_variants(8,5)/N
M3 = ysl_variants(8,6)/N
M4 = ysl_variants(8,7)/N
MU3 = (M3-3*M1*M2+2*M1^3)*h^3
MU4 = (M4-4*M1*M3+6*M1^2*M2-3*M1^4)*h^4
x_vib_yv = M1*h + 131.6
disp_yv = (M2-M1^2)*h^2
vib_sko_yv = sqrt(disp_yv)
assim_yv = MU3/vib_sko_yv^3
eksc_yv = MU4/vib_sko_yv^4-3

