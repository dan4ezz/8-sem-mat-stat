function [a]=dz4step0(k)
	a=randn(100,2);
	a(:,1)=a(:,1)+(50*rand(1,1)-25);
	a(:,2)=a(:,2)+(50*rand(1,1)-25);
	for i=1:(k-1)   %generation matrix
		b=randn(100,2);
		b(:,1)=b(:,1)+(50*rand(1,1)-25);
		b(:,2)=b(:,2)+(50*rand(1,1)-25);
		a=[a;b];
	end
	okno=figure();
	hold on
	plot(a'(1,:),a'(2,:),'rx');  %draw points
	hold off
	input("next - 1 ");
	delete(okno);
