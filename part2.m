vib_o = [viborka1; viborka2];
rangirx = sort(viborka1);
rangiry = sort(viborka2);
razmahx = rangirx(1, end) - rangirx(1, 1)
hx = razmahx / k;
razmahy = rangiry(1, end) - rangiry(1, 1)
hy = razmahy / k;


%по горизонтали х, по вертикали у
freq_table = zeros(k, k);

for i=1:N
	for j=1:k
		if (and(vib_o(1, i) > rangirx(1,1)+hx*(j-1), vib_o(1, i) <= rangirx(1,1)+hx*j)) %для х
			jx = j;
		endif
		if(vib_o(1, i) == rangirx(1,1))
			jx = 1;
		endif
		if (and(vib_o(2, i) > rangiry(1,1)+hy*(j-1), vib_o(2, i) <= rangiry(1,1)+hy*j)) %для y
			jy = j;
		endif
		if(vib_o(2, i) == rangiry(1,1))
			jy = 1;
		endif
	endfor

	freq_table(jy,jx) = freq_table(jy,jx) + 1;
endfor
freq_table

printf("rangir\n");
Exy = sum(vib_o(1,:).*vib_o(2,:), 2) / N
Ex = vib_sr
Ey = vib_sr2
ax = (Exy - Ex*Ey) / vib_sko2^2
ay = (Exy - Ex*Ey) / vib_sko^2
printf("Yp-e regr:\n\ty=%f+%f*(x-%f)\n\tx=%f+%f*(y-%f)\n", vib_sr2, ay, vib_sr, vib_sr, ax, vib_sr2);
r = (Exy - Ex*Ey) / (vib_sko*vib_sko2)
r_interval = [r - 3*(1-r^2)/sqrt(N), r + 3*(1-r^2)/sqrt(N)]


printf("interv\n");
Exy = sum(sum(freq_table.*(intervalniy(2,:)'*intervalniy2(2,:)), 2),1) / N
Ex = vib_sr_int
Ey = vib_sr_int2
ax = (Exy - Ex*Ey) / vib_sko2^2
ay = (Exy - Ex*Ey) / vib_sko^2
printf("Yp-e regr:\n\ty=%f+%f*(x-%f)\n\tx=%f+%f*(y-%f)\n", vib_sr_int2, ay, vib_sr_int, vib_sr_int, ax, vib_sr_int2);
r = (Exy - Ex*Ey) / (vib_sko_int*vib_sko_int2)
r_interval = [r - 3*(1-r^2)/sqrt(N), r + 3*(1-r^2)/sqrt(N)]
