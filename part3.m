clc; 
clear;
% rad of cluster
R = 40;
% number of clusters
N = 0;
size_before = 0;
size_after = 0;
errors_sgush = 0;
colour = ['b', 'r', 'g', 'c', 'm', 'k', 'b', 'r', 'g', 'c', 'y', 'm', 'k'];

init_data;

a =  [153 284 389 47 128 355 115 68 367 331 6 315 155 167 265 29 328 10 38 89 232 377 143 190 209 105 55 333 207 181 160 36 218 7 299 93 380 351 80 242 269 130 290 174 103 304 390 343 69 336 376 317 54 177 205 352 192 282 357 168 187 309 350 17 279 303];

viborka1=data1(1,a(1,:));
viborka2=data2(1,a(1,:));

viborka = [viborka1; viborka2];


viborka_sgush = viborka;
vib_size = size(viborka,2); 
viborka_sgush = [viborka_sgush; zeros(1, vib_size)];
viborka_sgush(3, :) = zeros(1, vib_size);
center = [];
%definition of clusters by condensations
while (size(find(viborka_sgush(3,:) == 0), 2) != 0) 
	% New cluster
	N ++;
	%Selecting the current centroid
	do
		el = fix((vib_size - 1) * rand (1, 1) + 1);
		center(:, N) = viborka_sgush(1:2, el);
	until (viborka_sgush(3, el) == 0)
	for i = 0:N
		q = find(viborka_sgush(3,:) == i);
	end;
	size_before = 0;
	size_after = 0;
	do	
		size_before = size_after;
		% distance to centroid
		viborka_sgush(4,:) = sqrt((center(1,N)*ones(1, vib_size)-viborka_sgush(1,:)).^2 + (center(2,N)*ones(1, vib_size)-viborka_sgush(2,:)).^2);
		% select point to cluster
		for j = 1:vib_size
			if (viborka_sgush(4, j) <= R && viborka_sgush(3, j) == 0)
				viborka_sgush(3, j) = N;
			end;
		end;	
		size_after = size(q, 2);
		
	until (size_before == size_after);
		%find errors
	errors_sgush = errors_sgush + sum(viborka_sgush(4, find(viborka_sgush(3, :) == N)));
end;
%results sgush
plot([],[]);
hold on
for i = 0:N
	q = find(viborka_sgush(3,:) == i);
	plot(viborka_sgush(1, q), viborka_sgush(2, q), [colour(i+1), 'x'], 'markersize', 5)
end;
q = find(viborka_sgush(3, :) == N);
if (size(q,2) != 0)
	center(1, N) = sum(viborka_sgush(1,q))/size(q,2);
	center(2, N) = sum(viborka_sgush(2,q))/size(q,2);
end;
plot(center(1, :), center(2, :), 'b*', 'markersize', 10)
plot(center(1, :), center(2, :), 'ro', 'markersize', 10)
hold off
errors_sgush
N
%-----------------------------------------
%-----------------------------------------

if (0 == 0)
		cluster = input("For watch next metod - <Enter>");
end;
%-----------------------------------------
%-----------------------------------------
%definition of clusters by k-means
errors = 0;
cluster = [];
errors_k_med = 0;
errors_before = 0;
errors_after = 0;
delta = 0.1;
viborka_k_med = viborka;
	%The choice of initial centroids
for q = 1:N
	el = fix((vib_size-1) * rand (1, 1))+1;
	center(1, q) = viborka_k_med(1, el);
	center(2, q) = viborka_k_med(2, el);
end;
do
	errors_before = errors_after;
	% Determination of the nearest centroid
	dist = sqrt((center(1,1)*ones(1, vib_size)-viborka_k_med(1,:)).^2 + (center(2,1)*ones(1, vib_size)-viborka_k_med(2,:)).^2);
	for w = 2:N
		dist = [dist;sqrt((center(1,w)*ones(1, vib_size)-viborka_k_med(1,:)).^2 + (center(2,w)*ones(1, vib_size)-viborka_k_med(2,:)).^2)];
	end;
	[m,im]=min(dist);
	viborka_k_med(3,:) = im;
	for i = 1:N
		q = find(viborka_k_med(3,:)==i);
		if (size(q,2) != 0)
			center(1,i) = sum(viborka_k_med(1,q))/size(q,2);
			center(2,i) = sum(viborka_k_med(2,q))/size(q,2);
		end;
	end;
	% current error
	errors_after = sum(m);
until (abs(errors_before-errors_after) <= delta);
%find error
errors_k_med = sum(m);
%results k-med
plot([],[]);
hold on;
for i = 1:N
	q = find(viborka_k_med(3,:) == i);
	plot(viborka_k_med(1, q), viborka_k_med(2, q), [colour(i), 'x'], 'markersize', 5)
end;
plot(center(1, :), center(2, :), 'b*', 'markersize', 10)
plot(center(1, :), center(2, :), 'ro', 'markersize', 10)
hold off;

errors_k_med

