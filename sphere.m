plot([],[]);
hold on;
hold off;

clc; 
clear;
cluster = [];
ResErr = 0;

R = 40;

delta = 1;

N = 0;

size_before = 0;
size_after = 0;

error = 0;

init_data;

a =  [153 284 389 47 128 355 115 68 367 331 6 315 155 167 265 29 328 10 38 89 232 377 143 190 209 105 55 333 207 181 160 36 218 7 299 93 380 351 80 242 269 130 290 174 103 304 390 343 69 336 376 317 54 177 205 352 192 282 357 168 187 309 350 17 279 303];

viborka1=data1(1,a(1,:));
viborka2=data2(1,a(1,:));

viborka = [viborka1; viborka2];

coordinate = [
				486, 487, 457, 470, 362, 392, 401, 353, 428, 538, 505, 331, 390, 443, 422, 528, 404, 463, 426, 467, 443, 463, 421, 547, 443, 457, 490, 477, 475, 557, 482, 458, 436, 410, 516, 434, 525, 483, 442, 460, 461, 458, 525, 593, 372, 523, 478, 405, 489, 465, 450, 510, 475, 320, 480, 541, 503, 536, 466, 498, 436, 400, 572, 453, 546, 493, 403, 412, 320, 391, 523, 505, 406, 418, 471, 418, 383, 430, 540, 426, 448, 585, 454, 449, 440, 392, 395, 500, 496, 518, 510, 398, 480, 502, 477, 369, 496, 510, 358, 553;
				139.4, 146, 103.6, 124, 111.7, 106.6, 100.1, 98, 113.7, 165, 137.5, 74.1, 108.1, 135.7, 122.9, 163.4, 112, 129.1, 119, 113, 122.9, 129.2, 118, 154.7, 121.9, 126.4, 134.4, 146, 132, 151.9, 139.9, 121.7, 114.3, 110.5, 132.6, 118.6, 165.9, 130.3, 123.4, 136.8, 134.9, 124.4, 156.5, 187.4, 81.7, 152.6, 136.6, 107.5, 149.8, 114.8, 122.3, 162.3, 143.6, 72.6, 153.9, 146.8, 149.9, 181, 130.3, 145.5, 117.9, 106.3, 180.9, 126.4, 177, 149.7, 123.9, 116.3, 64.5, 107.5, 148.7, 155.8, 113.8, 118.6, 119.7, 125.7, 109.7, 95.3, 156.7, 128.2, 137.3, 177.7, 131.1, 124.5, 126.7, 82.7, 109.1, 155.5, 141.7, 144.4, 140.6, 109, 117.7, 122.6, 139.7, 84.3, 155.3, 124.1, 98.3, 159.1
				];

coordinate = [coordinate; zeros(2, 100)];

for k = 1:5
	k
	
	coordinate(3, :) = zeros(1, 100);
	coordinate(5, :) = zeros(1, 100);
	center = [];
	error = 0;
	N = 0;
	while (size(find(coordinate(3,:) == 0), 2) != 0)
		
		
		N ++;
		
		do
			el = fix(99 * rand (1, 1) + 1);
			center(:, N) = coordinate(1:2, el);
		until (coordinate(3, el) == 0)
		
		plot([],[]);
		hold on
		colour = ['b', 'r', 'g', 'c', 'y', 'm', 'k', 'b', 'r', 'g', 'c', 'y', 'm', 'k'];
		for i = 0:N
			q = find(coordinate(3,:) == i);
			plot(coordinate(1, q), coordinate(2, q), [colour(i+1), '.'], 'markersize', 5)
		end;
		plot(center(1,:), center(2,:), 'r*', 'markersize', 10)
		hold off
		
		size_before = 0;
		size_after = 0;
		
		do	
			size_before = size_after;
			
	
			if (size(cluster) == 0)
				cluster = input("For watch next step - <Enter>, for watch result - Any Number: ");
			end;

	
			coordinate(4,:) = sqrt((center(1,N)*ones(1, 100)-coordinate(1,:)).^2 + (center(2,N)*ones(1, 100)-coordinate(2,:)).^2);
			
	
			e = find(coordinate(5,:) == N);
			coordinate(5, e) = 0;
			for j = 1:100
				if (coordinate(4, j) <= R && coordinate(3, j) == 0)
					coordinate(5, j) = N;
				end;
			end;
			
	
			plot([],[]);
			hold on
			colour = ['b', 'r', 'g', 'c', 'y', 'm', 'k', 'b', 'r', 'g', 'c', 'y', 'm', 'k'];
			for i = 0:N
				q = find(coordinate(5,:) == i);
				plot(coordinate(1, q), coordinate(2, q), [colour(i+1), '.'], 'markersize', 5)
			end;
			plot(center(1, :), center(2, :), 'b*', 'markersize', 10)
			hold off
			
	
			q = find(coordinate(5, :) == N);
			size_after = size(q, 2);
			
	
			if (size(q,2) != 0 && size_before != size_after)
				center(1, N) = sum(coordinate(1,q))/size(q,2);
				center(2, N) = sum(coordinate(2,q))/size(q,2);
			end;
			
			if (size_before == size_after)
				coordinate(3, :) = coordinate(5, :);
			end;
			
			
		until (size_before == size_after);
	
		
		error = error + sum(coordinate(4, find(coordinate(3, :) == N)));
	end;
	
	error
	
	if (ResErr == 0)
		ResErr = error;
		ResKl = coordinate;
		ResCen = center;
		ResN = N;
	else
		if (error < ResErr)
			ResErr = error;
			ResKl = coordinate;
			ResCen = center;
			ResN = N;
		end;
	end;
end;


plot([],[]);
hold on;
colour = ['b', 'r', 'g', 'c', 'y', 'm', 'k', 'b', 'r', 'g', 'c', 'y', 'm', 'k'];
for i = 1:ResN
	q = find(ResKl(3,:)==i);
	plot(ResKl(1, q), ResKl(2, q), [colour(i), '.'], 'markersize', 5)
end;
plot(ResCen(1, :), ResCen(2, :), 'b*', 'markersize', 10)
hold off;

ResErr
ResN
